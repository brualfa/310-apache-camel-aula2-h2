package com.example.ApacheCamelAula2.rotas;

import java.io.File;

import org.apache.camel.Exchange;
import org.apache.camel.Predicate;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.file.GenericFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.example.ApacheCamelAula2.services.ClienteService;

@Component
public class ImportarClienteRoute extends RouteBuilder{

	// A classe RouteBuilder me obriga a escrever o método configure pois possui um metodo abstrato
	
	@Autowired
	private ClienteService service;
	
	@Override
	public void configure() throws Exception{
		
		Predicate csvValido = new Predicate() {
			
			@Override
			public boolean matches(Exchange exchange) {

				return ImportarClienteRoute.this.service.isCsv(
						(File)((GenericFile) exchange.getIn().getBody()).getFile());
			}
		};
		
		from("file://origem/?noop=true")
		.routeId("rota-importacao")
		.choice()  //se ele não cair no When ele vai para o otherwise
		.when(csvValido)
		.process(new Processor() {
			
			@Override
			public void process(Exchange exchange) throws Exception {
				String nomeArquivo = exchange.getIn().getHeader(Exchange.FILE_PATH, String.class);
				
				System.out.printf("\nTentando importar o arquivo %s \n", nomeArquivo);
				
			}
		})  // Componente do camel chamado bean - só é possível utilizar porque estamos com o Spring como dependencia.
		// o Spring da um nome para a classe ClienteService porque ela foi anotada como @Service, com caracteristicas de variável "clienteService".
		// ${ (chamada de expressão), 
		.to("bean:clienteService?method=importarCliente(${body.getFile()})")
		.otherwise()
		.log("Arquivo ${body.getAbsoluteFilePath()} não é um CSV Válido")
		.end();
	}
}
