package com.example.ApacheCamelAula2.rotas;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.ApacheCamelAula2.services.BairroService;

@Component
public class ImportarBairroRoute extends RouteBuilder{
	
	@Autowired
	private BairroService bairroService;
	
	@Override
	public void configure() throws Exception{
		
		from("file://origem2/?noop=true")
		.routeId("rota-importacao-bairro").process(new Processor() {
			
			@Override
			public void process(Exchange exchange) throws Exception {
				String nomeArquivo = exchange.getIn().getHeader(Exchange.FILE_PATH, String.class);
				
				System.out.printf("\nTentando importar o arquivo %s \n", nomeArquivo);
			}
		})
		.to("bean:bairroService?method=importarBairro(${body.getFile()})")
		.end();
	}
	
}
