	package com.example.ApacheCamelAula2.Repository;

import org.springframework.data.repository.CrudRepository;

import com.example.ApacheCamelAula2.entidades.Bairro;

public interface BairroRepository extends CrudRepository<Bairro, Integer>{

}
