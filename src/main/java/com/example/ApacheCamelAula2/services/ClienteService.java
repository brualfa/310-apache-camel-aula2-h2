package com.example.ApacheCamelAula2.services;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.EntityManager;


import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.ApacheCamelAula2.entidades.Cliente;
import com.example.ApacheCamelAula2.modelos.ClienteImportacao;

@Service
public class ClienteService {

	//Transação da JPA
	@Autowired
	private EntityManager em;
	
	
	@Transactional
	public void salvarCliente(ClienteImportacao novoCliente) {
		Integer novoId = ((Long)
				em.createQuery("select count(c)+1 from Cliente c")
				.getSingleResult()).intValue();
				
		Cliente cliente = novoCliente.criarCliente();
		//cliente.setId(novoId);
		
		em.persist(cliente);
	}
	
	//Este método tem que ser transacional para conseguir persistir no banco.
	@Transactional
	public void importarCliente(File arquivo) throws IOException {
		
		CSVParser csv = CSVFormat.DEFAULT.parse(new FileReader(arquivo));
		
		for (CSVRecord linha : csv) {
			ClienteImportacao novoCliente = this.criarCliente(linha);
			this.salvarCliente(novoCliente);
		}
	}
	
	private ClienteImportacao criarCliente(CSVRecord linha) {
		String nome = linha.get(0);
		LocalDate nascimento = LocalDate.parse(linha.get(1));
		BigDecimal saldo = new BigDecimal(linha.get(2));
		int idBairro = new Integer(linha.get(3));
		
		//chamado de objeto homonimo -- Se trata do construtor
		return new ClienteImportacao(nome, nascimento, saldo, idBairro);
	}
	
	@Transactional
	public boolean isCsv(File arquivo) {
		try {
			criarCliente(CSVFormat.DEFAULT.parse(new FileReader(arquivo)).getRecords().get(0));
			return true;
		} catch (Exception e) {
			return false;
		}
	}
}
